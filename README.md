[METAHEAP](https://gitlab.com/metaheap/metaheap) README.md
========

# [INFRASTRUCTURE](https://gitlab.com/metaheap/metaheap-infrastructure): [![pipeline status](https://gitlab.com/metaheap/metaheap-infrastructure/badges/master/pipeline.svg)](https://gitlab.com/metaheap/metaheap-infrastructure/commits/master)
# [FRONTEND](https://gitlab.com/metaheap/metaheap-frontend): [![pipeline status](https://gitlab.com/metaheap/metaheap-frontend/badges/master/pipeline.svg)](https://gitlab.com/metaheap/metaheap-frontend/commits/master)
# [TRABUR](https://gitlab.com/metaheap/metaheap-trabur): [![pipeline status](https://gitlab.com/metaheap/metaheap-trabur/badges/master/pipeline.svg)](https://gitlab.com/metaheap/metaheap-trabur/commits/master)

#### Deploying Development
[Meta](https://www.npmjs.com/package/meta) is a tool for managing multi-project systems and libraries. This project uses it under the hood for microservices. 
```bash
# if you don't aleady have meta installed globaly ($ meta --version)
# then run the following...
$ npm i -g meta

# you can initialize a new git repository and install meta to it
$ meta init

# add a microservice to the list:
# meta project add <microservice> <git>
$ meta project add frontend git@gitlab.com:metaheap/metaheap-frontend.git
```

Let's start out by installing the project on to our local machine:
```bash
# start
$ cd ~/Projects

# copy
$ git clone git@gitlab.com:metaheap/metaheap.git

# jump into it
$ cd metaheap

# install root deps
$ npm install

# get microservices
$ meta git update

# show
$ ls -la
```

After completion... You should see the following:
- [`frontend`](https://gitlab.com/metaheap/metaheap-frontend) Next.js Website
- [`proxy`](https://gitlab.com/metaheap/metaheap-proxy) Proxy/Load Balancer for Development
- [`trabur`](https://gitlab.com/metaheap/metaheap-trabur) Next.js Website

Install node.js just as you would normally with install but prefix commands with meta; doing so will replicate commands across all the microservices. Use begin and end for developing the app behind pm2 which has nice features like file watching that will auto restart the service on file change.
```bash
# install microservice(s) deps
$ meta npm install

# build next.js projects
$ meta npm run build --include-only frontend,trabur

# if you don't already have PM2 installed ($ pm2 --version)
# then you will need to run this before you continue:
$ npm install pm2 -g

# start project development
$ meta npm run begin

# stop project development
$ meta npm run end
```

> note: meta npm is a meta plugin and only works if `npm i meta-npm --save-dev` has been installed to npm's package.json under devDependencies.

Once all of the services are running, the following will be accessible at these URLs:
- frontend: Next.js Website http://localhost:8374
- trabur: Next.js Website http://localhost:7548

You could open up the browser and hit those URL's as they are but you will want to modify the hosts file to use development URL's (like http://metaheap.development) instead. To do this, just use `npm i hostile -g` to install hostile then run the following commands...
```bash
# add development domains to hosts file
$ sudo hostile load hosts.txt

# remove development domains from hosts file
$ sudo hostile unload hosts.txt
```

There is a proxy running on port 8080 to loadbalance traffic to each of the services. We need this to be behind port 80 for it to work. Running anything on ports less than 1024 requires sudo. So instead we'll modify the OS's config. Use the following command(s) to forward port 80 to 8080.
```bash
# if you haven't already, redirect port 80 to 8080 (linux)
# https://askubuntu.com/questions/444729/redirect-port-80-to-8080-and-make-it-work-on-local-machine
$ iptables -t nat -A OUTPUT -o lo -p tcp --dport 80 -j REDIRECT --to-port 8080

# here is the same thing (mac os)
# https://apple.stackexchange.com/questions/230300/what-is-the-modern-way-to-do-port-forwarding-on-el-capitan-forward-port-80-to
$ echo "
rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080
" | sudo pfctl -ef -
```

You should now be able to access microservices by using development domain name URLs:
- frontend: Next.js Website http://www.metaheap.development
- trabur: Next.js Website http://trabur.metaheap.development

Congrats! You should now see a successfully installed and running application on your local machine. But this is intented for development purposes. Later, we'll get into deploying to production. For now, here are few handy commands to use while developing.
```bash
# after running 'npm run begin' check to make sure things are ok
$ pm2 status

# see a problem? dig into a microservice and find out why
# pm2 logs <microservice>
$ pm2 logs metaheap-proxy --lines 1000

# add a microservice to the list:
# meta project add <microservice> <git>
$ meta project add backend git@gitlab.com:metaheap/metaheap-backend.git
```

#### Deploying Production
Just in case you want to install kubernetes applications to a new gitlab repo when they already have been installed; run these commands before you press install and it should work fine.
```bash
$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" delete namespaces gitlab-managed-apps
namespace "gitlab-managed-apps" deleted

$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" delete customresourcedefinitions.apiextensions.k8s.io certificates.certmanager.k8s.io clusterissuers.certmanager.k8s.io issuers.certmanager.k8s.io
customresourcedefinition.apiextensions.k8s.io "certificates.certmanager.k8s.io" deleted
customresourcedefinition.apiextensions.k8s.io "clusterissuers.certmanager.k8s.io" deleted
customresourcedefinition.apiextensions.k8s.io "issuers.certmanager.k8s.io" deleted
```

If you get job failed when deploying to production with errors that look like the following:
```
Error from server (Forbidden): namespaces "fleetgrid" is forbidden: User "system:serviceaccount:fleetgrid:fleetgrid-service-account" cannot get resource "namespaces" in API group "" in the namespace "fleetgrid"
Error from server (Forbidden): namespaces is forbidden: User "system:serviceaccount:fleetgrid:fleetgrid-service-account" cannot create resource "namespaces" in API group "" at the cluster scope
```

It is becuase the pipline is tring to deply the project under the namespace (ns is setup during cluster config) and it doesn't have permission. Fix it with the following command and redeploy the pipeline:
```bash
# kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" create clusterrolebinding --user system:serviceaccount:<namespace>:<namespace>-service-account <namespace>-service-account-gitlab-sa-admin --clusterrole cluster-admin

# example 1
$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" create clusterrolebinding --user system:serviceaccount:fleetgrid:fleetgrid-service-account fleetgrid-service-account-gitlab-sa-admin --clusterrole cluster-admin

# example 2
$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" create clusterrolebinding --user system:serviceaccount:metaheap-trabur:metaheap-trabur-service-account metaheap-trabur-service-account-gitlab-sa-admin --clusterrole cluster-admin

# show a list of CRB
$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" get clusterrolebinding

# delete CRB
$ kubectl --kubeconfig="fleetgrid-kubeconfig.yaml" delete clusterrolebinding metaheap-trabur-service-account-gitlab-sa-admin
```